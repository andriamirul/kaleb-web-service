## Kaleb Web Service

Kaleb Web Service is a Vue.js-based application designed to provide user authentication and profile management features. The application consists of the following pages:

### Getting Started

1. **Set up the environment**: Copy the `example.env` file to `.env` and configure the database host and other environment variables to match your local setup.
   `cp example.env .env`
2. Install the required packages by running:
    ```yarn install```
3. Start the project with: 
    ```yarn dev```

### Usage

1. Navigate to `http://localhost:5173/register` to create a new account.
2. Navigate to `http://localhost:5173/login` to log in with your existing account.
3. Navigate to `http://localhost:5173/profile` to view and edit your profile after logging in.
4. If you access an unknown path, you will be redirected to the 404 page.
